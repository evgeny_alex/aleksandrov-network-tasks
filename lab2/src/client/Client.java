 	import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class Client {
    private InetAddress address;
    private int port;
    private Socket clientSocket;
    private BufferedInputStream in;
    private BufferedOutputStream out;
    private File file;
    private FileInputStream fileInputStream;

    public static void main(String[] args){
        Client client = new Client(args[1], args[2]);
        client.send(args[0]);
    }
    private Client(String _address, String _port){
        try {
            address = InetAddress.getByName(_address);
            port = Integer.parseInt(_port);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    private void send(String path){
        try {
            file = new File(path);
            clientSocket = new Socket(address, port);
            in = new BufferedInputStream(clientSocket.getInputStream());
            out = new BufferedOutputStream(clientSocket.getOutputStream());
            fileInputStream = new FileInputStream(file);
            System.out.println("Клиент подключился к серверу");

            sendSizeFile();
            sendNameFile();
            sendFile();
            recvByte();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                clientSocket.close();
                in.close();
                out.close();
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void sendSizeFile() throws IOException {
        long sizeFile = file.length();
        ByteBuffer byteBuffer = ByteBuffer.allocate(Long.BYTES);
        byteBuffer.putLong(sizeFile);
        byte[] bufSize = byteBuffer.array(); // буфер для передачи размера файла

        out.write(bufSize, 0, Long.BYTES);
        out.flush();
    }
    private void sendNameFile() throws IOException {
        String nameFile = file.getName();
        byte[] buf = nameFile.getBytes(StandardCharsets.UTF_8);

        ByteBuffer byteBuffer = ByteBuffer.allocate(Integer.BYTES);
        byteBuffer.putInt(nameFile.length());
        byte[] bufLen = byteBuffer.array();

        out.write(bufLen, 0, Integer.BYTES); // передаем длину имени
        out.flush();

        out.write(buf); // передаем имя
        out.flush();
    }
    private void sendFile() throws IOException {
        byte[] buf = new byte[8];
        int count;
        while ((count = fileInputStream.read(buf)) > 0) {
            out.write(buf, 0, count);
            out.flush();
        }
    }
    private void recvByte() throws IOException {
        byte[] buf = new byte[1];
        in.read(buf, 0, 1);
        String message;
        if (buf[0] == 1) message = "Удалось передать файл.";
        else message = "Не удалось передать файл.";
        System.out.println(message);
    }
}
