import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private int port;   // порт, который будет прослушивать наш сервер

    public static void main(String[] args) {
        Server server = new Server(args[0]);
        server.start();
    }

    public Server(String _port) {
        port = Integer.parseInt(_port);
    }
    public void start(){
        Socket clientSocket = null; // сокет клиента
        ServerSocket serverSocket = null; // серверный сокет
        try {
            serverSocket = new ServerSocket(port); // создаём серверный сокет на определенном порту
            System.out.println("Сервер запущен!");
            
            while (true) {
                clientSocket = serverSocket.accept();   // таким образом ждём подключений от сервера
                ClientHandler client = new ClientHandler(clientSocket);   // this - это наш сервер
                new Thread(client).start(); // каждое подключение клиента обрабатываем в новом потоке
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                clientSocket.close();
                serverSocket.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
