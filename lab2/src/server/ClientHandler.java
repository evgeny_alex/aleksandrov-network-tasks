import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class ClientHandler implements Runnable {
    private Socket clientSocket;
    private BufferedInputStream in;
    private BufferedOutputStream out;
    private long sizeFile;
    private String nameFile;
    private FileOutputStream fileOutputStream;
    private int countRecvByte;
    private int countRecvByteBefore;
    private int allTime;
    private String address;
    private boolean flagEnd = false;


    public ClientHandler(Socket _socket){
        clientSocket = _socket;
        countRecvByte = 0;
        countRecvByteBefore = 0;
        allTime = 0;
        address = clientSocket.getRemoteSocketAddress().toString();
        try {
            in = new BufferedInputStream(clientSocket.getInputStream());
            out = new BufferedOutputStream(clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            System.out.println("Сервер обрабатывает клиента " + address);
            recvSizeFile(); // принимаем размер файла
            recvNameFile(); // принимаем имя файла
            recvFile();     // принимаем содержимое файла
            sendByte();     // передаем байт об успехе/неуспехе операции
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                clientSocket.close();
                in.close();
                out.close();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Клиент " + address + " отключился");
        }
    }

    private void recvSizeFile() throws IOException {
        byte[] bufSize = new byte[8];
        in.read(bufSize, 0, 8);
        ByteBuffer byteBuffer = ByteBuffer.allocate(Long.BYTES);
        byteBuffer.put(bufSize);
        byteBuffer.flip();

        sizeFile = byteBuffer.getLong();
    }
    private void recvNameFile() throws IOException {
        byte[] bufLen = new byte[4];
        in.read(bufLen, 0, 4);
        ByteBuffer byteBuffer = ByteBuffer.allocate(Integer.BYTES);
        byteBuffer.put(bufLen);
        byteBuffer.flip();
        int lengthName = byteBuffer.getInt();

        byte[] buf = new byte[lengthName];
        in.read(buf, 0, lengthName);
        nameFile = new String(buf, StandardCharsets.UTF_8);
    }
    private void recvFile() throws IOException {
        File file = new File("C://uploads/"+nameFile);
        file.createNewFile();
        fileOutputStream = new FileOutputStream(file);

        byte[] buf = new byte[8];
        int count;
        printSpeed();
        while ((count = in.read(buf)) > 0) {
            countRecvByte += count;
            fileOutputStream.write(buf, 0, count);
            fileOutputStream.flush();
            if (countRecvByte == sizeFile) break;
        }
        flagEnd = true;
    }
    private void sendByte() throws IOException {
        byte[] b = new byte[1];
        if (countRecvByte == sizeFile) b[0] = 1;
        out.write(b);
        out.flush();
    }
    private void printSpeed(){
        new Thread(()->{
            while (true){
                try {
                    Thread.sleep(3000);
                    allTime+=3;
                    System.out.println("Мгновенная скорость " + address + " : " + getInstantSpeed());
                    System.out.println("Средняя скорость " + address + " : " + getAverageSpeed());
                    countRecvByteBefore = countRecvByte;
                    if (flagEnd) break;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private String getAverageSpeed(){
        return countRecvByte/allTime + " byte/sec";
    }
    private String getInstantSpeed(){
        return (countRecvByte - countRecvByteBefore)/3 + " byte/sec";
    }
}
