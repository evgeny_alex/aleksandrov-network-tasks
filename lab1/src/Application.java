import java.io.IOException;
import java.net.*;

public class Application implements Runnable {
    private MulticastSocket multicastSocketRecv = null;
    private MulticastSocket multicastSocketSend = null;
    private String address;
    private int port = 1234;
    private byte[] bufRecv = new byte[256];
    private byte[] bufSend = new byte[256];
    private Storage storage = new Storage();
    private int timeout = 3000;
    private boolean flagEnd = false;
    private InetAddress group;

    public static void main(String[] args) {
        Application application = new Application(args[0]);
        new Thread(application).start();
    }

    private Application(String _address){
        address = _address;
    }

    @Override
    public void run() {
        try {
            multicastSocketRecv = new MulticastSocket(port); // создаем сокет для приема пакетов
            group = InetAddress.getByName(address);
            multicastSocketRecv.joinGroup(group);
            multicastSocketRecv.setSoTimeout(2000);// устанавливаем таймаут на 10 секунд

            multicastSocketSend = new MulticastSocket(); // создаем сокет для передачи пакетов
            String message = "Hello group!";
            bufSend = message.getBytes();

            while(true){ // прием и передача пакетов
                sendPacket(group);

                long startTime = System.currentTimeMillis();
                long endTime = System.currentTimeMillis();
                while (endTime - startTime <= timeout) {
                    DatagramPacket packetRecv = new DatagramPacket(bufRecv, bufRecv.length);
                    try {
                        multicastSocketRecv.receive(packetRecv);
                    } catch (SocketTimeoutException ignored) { // если ничего не приняли, продолжаем передавать пакеты по мультикасту
                        endTime = System.currentTimeMillis();
                        continue;
                    }
                    storage.addAddress(packetRecv.getAddress().getHostAddress() + ":" + packetRecv.getPort()); // добавляем
                    
                    endTime = System.currentTimeMillis();
                }
                storage.checkStorage();
                storage.cleanUp(); // очищаем текущее хранилище
                
            }

            
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                multicastSocketRecv.leaveGroup(group);
            } catch (IOException e) {
                e.printStackTrace();
            }
            multicastSocketRecv.close();
            multicastSocketSend.close();
        }
    }
    private void sendPacket (InetAddress group) throws IOException {
        DatagramPacket packetSend = new DatagramPacket(bufSend, bufSend.length, group, port);
        multicastSocketSend.send(packetSend);
    }
}
