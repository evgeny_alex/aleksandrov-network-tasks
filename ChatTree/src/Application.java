import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Application {
    private String nameNode;
    private String localAddress;
    private int portNode;
    private int percentLoss;
    private DatagramSocket sendSocket;
    private DatagramSocket recvSocket;
    private byte[] bufRecv = new byte[256];
    private List<Node> list = new CopyOnWriteArrayList<>();
    private Scanner in = new Scanner(System.in);
    private static final int SIZE = 100;
    private List<MessageAccount> accountList = new CopyOnWriteArrayList<>(); // учет сообщений
    private Node node;

    public static void main(String[] args) {
        Application application = new Application(args);
        application.start();
    }

    private Application(String[] args) {
        try {
            nameNode = args[0];
            portNode = Integer.parseInt(args[1]);
            percentLoss = Integer.parseInt(args[2]);
            localAddress = getLocalIpAddress();
            recvSocket = new DatagramSocket(portNode, InetAddress.getByName(localAddress));
            sendSocket = new DatagramSocket();
            //recvSocket.setReuseAddress(true);
            //sendSocket.setReuseAddress(true);

            node = new Node(localAddress, portNode);
            if (args.length > 3) {
                Node node = new Node(args[3], Integer.parseInt(args[4]));
                list.add(node);
                Message message = new Message("registration", "0", node.getData());
                sendPacket(node, message); // отправляем в пакете, своего зама
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void start() {
        printMessage(); // печатаем и отправляем сообщения
        pingNeighbour();
        recvMessage();  // принимаем пакеты
    }

    private void printMessage() { // печать сообщения с клавиатуры и рассылка его всем соседям
        new Thread(() -> {
            String data;
            String uid;
            while (in.hasNextLine()) {
                data = nameNode + '\n' + in.nextLine();
                uid = UUID.randomUUID().toString();
                Message message = new Message("message", uid, data);

                newMessage(message, node);
                for (Node n : list) {
                    try {
                        sendPacket(n, message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
    private void recvMessage() {
        try {
          	while (true) {
	            DatagramPacket packetRecv = new DatagramPacket(bufRecv, bufRecv.length);
	            recvSocket.receive(packetRecv);
	            packetHandler(packetRecv);
        	}
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        	freeResources();
        }
    }
    private void pingNeighbour() {
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(15000);
                    for (Node n : list) {
                        if (!n.getActive()) {
                            if (list.indexOf(n) == 0) rebuildTree(); // если зам отключился, перестроение
                            System.out.println("Узел " + n.getAddress() + " отлючился.");
                            list.remove(n);
                            continue;
                        }
                        n.setActive(false); // устанавливаем неактивность и отправляем пинг
                    }
                    System.out.println("Количество соседей = " + list.size());
                    for (MessageAccount m : accountList) { // рассылаем по новой сообщения из списка
                        for (Node n : list) {
                            if (n == m.getNode()) continue; // на тот же узел, с которого пришло сообщение не отправляем
                            sendPacket(n, m.getMessage());
                        }
                    }
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private void freeResources(){
        sendSocket.close();
        recvSocket.close();
        list.clear();
        in.close();
        accountList.clear();
    }

    private void packetHandler(DatagramPacket packetRecv) {
        bufRecv = new byte[256];
        try {
            Message message = parsePacket(new String(packetRecv.getData()));
            if (Math.random() * 99 < percentLoss) return;
            System.out.println("Пришел пакет <" + message.getHeader() + "> from " + packetRecv.getAddress().getHostAddress());
            System.out.println("c данными : " + message.getData());

            if (message.getHeader().equals("registration")) registrationHandler(packetRecv, message);
            if (message.getHeader().equals("answer")) answerHandler(message); // пакет ответа на регистрацию
            if (message.getHeader().equals("message")) messageHandler(packetRecv, message);
            if (message.getHeader().equals("confirmation")) confirmationHandler(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void registrationHandler(DatagramPacket packetRecv, Message message) throws IOException {
        String data = message.getData();
        String subAddress = "";
        String port = "";
        if (message.getUid().equals("0")) { // если просто регистрация
            int i;
            String portListen = "";
            for (i = 0; data.charAt(i) != '\n'; i++) portListen += data.charAt(i);
            Node node = new Node(packetRecv.getAddress().getHostAddress(), Integer.parseInt(portListen));// порт прослушивания должен передаваться в пакете, изменить!
            for (i = i + 1; data.charAt(i) != '\n'; i++) subAddress += data.charAt(i);
            for (i = i + 1; i < data.length(); i++) port += data.charAt(i);
            node.setSubNode(subAddress, Integer.parseInt(port));
            System.out.println("Новый сосед : " + node.getAddress() + " c замом " + subAddress);
            list.add(node);
            Message messageAns = new Message("answer", "0", list.get(0).getData());
            Message messageReg = new Message("registration", "1", node.getData());
            sendPacket(node, messageAns); // отправляем в пакете, своего зама
            for (Node n : list){ // каждому соседу говорим куда теперь подключаться, при моем отключении
                if (packetRecv.getAddress().getHostAddress().equals(n.getAddress())) {
                    continue;
                }
                sendPacket(n, messageReg); // отправляем в пакете, куда теперь нужно подключиться
            }
        } else { // иначе меняем зама соседа
            for (Node n : list){
                if (packetRecv.getAddress().getHostAddress().equals(n.getAddress())) {
                    int i;
                    for (i = 0; data.charAt(i) != '\n'; i++) subAddress += data.charAt(i);
                    for (i = i + 1; i < data.length(); i++) port += data.charAt(i);
                    n.setSubNode(subAddress, Integer.parseInt(port));
                }
            }
        }
    }
    private void answerHandler(Message message) {
        Node node = list.get(0);
        String subAddress = "";
        String port = "";
        int i;
        for (i = 0; message.getData().charAt(i) != '\n'; i++) subAddress += message.getData().charAt(i);
        for (i = i + 1; i < message.getData().length(); i++) port += message.getData().charAt(i);
        System.out.println("Новый зам у node: " + node.getAddress() + " - " + subAddress);
        node.setSubNode(subAddress, Integer.parseInt(port));
    }
    private void messageHandler(DatagramPacket packetRecv, Message message) throws IOException {
        String data = message.getData();
        String name = "", messageStr = "";
        for (MessageAccount m : accountList) { // проверяем есть ли сообщение в учете
            if (m.checkUID(message.getUid())) {
                return;
            }
        }
        boolean isNeighbour = false; // проверка на то, что сообщение пришло от соседа
        for (Node n : list) {
            if (n.getAddress().equals(packetRecv.getAddress().getHostAddress())) {
                isNeighbour = true;
                break;
            }
        }
        if (!isNeighbour) return;
        int i;
        for (i = 0; data.charAt(i) != '\n'; i++) name += data.charAt(i);
        for (i = i + 1; i < data.length(); i++) messageStr += data.charAt(i);
        System.out.println(name + ": " + messageStr);

        for (Node n : list) {
            if (packetRecv.getAddress().getHostAddress().equals(n.getAddress())) {
                newMessage(message, n); // добавляем принятое сообщение в учет, чтобы остальные соседи вернули подтверждение
                Message message1 = new Message("confirmation", message.getUid(), "");
                sendPacket(n, message1);
                n.setActive(true);
                continue;
            }
            sendPacket(n, message);
        }
    }
    private void confirmationHandler(Message message) {
        for (MessageAccount m : accountList) {
            if (m.checkUID(message.getUid())) {
                if (m.decrement() == 0) accountList.remove(m);
            }
        }
    }
    private Message parsePacket(String data) {
        String header = "";
        String dataMessage = "";
        String uid = "";
        int i;
        for (i = 0; data.charAt(i) != '\n'; i++)
            header += data.charAt(i);
        for (i = i + 1; data.charAt(i) != '\n'; i++)
            uid += data.charAt(i);
        for (i = i + 1; data.charAt(i) != '\0'; i++)
            dataMessage += data.charAt(i);

        Message message = new Message(header, uid, dataMessage);
        return message;
    }
    private void newMessage(Message message, Node node) {
        int a = list.size();
        if (a == 0) return; // если соседей нет, в учет ничего не добавляем
        if (message.getHeader().equals("ping")) a = 1;
        MessageAccount messageAccount = new MessageAccount(a, message, node);
        if (accountList.size() == SIZE) accountList.remove(0);
        accountList.add(messageAccount);
    }
    private void sendPacket(Node node, Message message) throws IOException {
        String header = message.getHeader();
        String uid = message.getUid();
        String data = message.getData();
        String str = header + "\n" + uid + "\n" + data;
        if (header.equals("registration") && uid.equals("0")) // при регистрации послылаем порт прослушивания
            str = header + "\n" + uid + "\n" + portNode + "\n" + data;
        SocketAddress socketAddress = new InetSocketAddress(InetAddress.getByName(node.getAddress()), node.getPort());
        DatagramPacket packetSend = new DatagramPacket(str.getBytes(), str.getBytes().length, socketAddress);
        sendSocket.send(packetSend);
    }
    private void rebuildTree() throws IOException {
        String address = list.get(0).getSubNodeAddress(); // получаем адрес зама
        System.out.println("local address: " + localAddress);
        System.out.println("neighbour sub node address: " + address + " from " + list.get(0).getAddress());

        if (address.equals(localAddress) || checkNeighbourInList(address) || address.equals("")) return; // если ты и есть зам, не перестраиваем
        System.out.println("Rebuilding tree.");
        int port = list.get(0).getSubNodePort(); // получаем порт
        Node node = new Node(address, port);
        Message message = new Message("registration", "0", node.getData());
        sendPacket(node, message);
        list.add(1, node); // вставляем новый узел вторым в лист, чтобы потом он стал замом
    }
    private boolean checkNeighbourInList(String address){
        for (Node n : list) {
            if (n.getAddress().equals(address)) return true;
        }
        return false;
    }

    private static String getLocalIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp() || iface.isVirtual())
                    continue;


                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    if (addr instanceof Inet4Address) {
                        ip = addr.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        return ip;
    }
}
