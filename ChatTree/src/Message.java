public class Message {
    private String header;
    private String uid;
    private String data;

    public Message(String header, String uid, String data){
        this.header = header;
        this.uid = uid;
        this.data = data;
    }

    public String getHeader() {
        return header;
    }

    public String getUid() {
        return uid;
    }

    public String getData() {
        return data;
    }
}
