public class MessageAccount {
    private int count;
    private Message message;
    private Node node; // узел с которого пришло сообщение

    public MessageAccount(int count, Message message, Node node){
        this.count = count;
        this.message = message;
        this.node = node;
    }

    public boolean checkUID(String uid){
        return (message.getUid().equals(uid));
    }

    public int decrement(){
        return --count;
    }
    public Message getMessage() {
        return message;
    }

    public Node getNode() {
        return node;
    }
}

