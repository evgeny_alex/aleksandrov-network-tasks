public class Node {
    private String subNodeAddress = ""; // заместитель узла (пара ip:port)
    private int subNodePort;
    private String address;
    private int port;
    private boolean active = true;

    public Node(String address, int port) {
        this.address = address;
        this.port = port;
    }
    public void setSubNode(String address, int port){
        subNodeAddress = address;
        subNodePort = port;
    }
    public String getData(){
        return address +"\n"+ port;
    }
    public String getAddress(){
        return address;
    }
    public int getPort(){
        return port;
    }
    public void setActive(boolean b){
        this.active = b;
    }
    public boolean getActive(){
        return active;
    }

    public int getSubNodePort() {
        return subNodePort;
    }

    public String getSubNodeAddress() {
        return subNodeAddress;
    }
}
